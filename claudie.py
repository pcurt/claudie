#!/usr/bin/python3.8

from argparse import ArgumentParser
import configparser
from datetime import datetime
import fitparse
import folium
from gpx_converter import Converter
import dateutil
import tcxparser
import os
from tqdm import tqdm
import pandas as pd


COLUMNS = ["Timestamp", "Latitude", "Longitude"]


def sem_to_deg(s):
    return s*180.0/2**31


def get_fit_timestamp(fitfile_name):
    global config
    try:
        fitfile = fitparse.FitFile(os.path.join(config["general"]["activities_directory"],
                                                fitfile_name))
    except fitparse.utils.FitHeaderError as e:
        raise Exception("Error parsing fit file {}".format(fitfile_name)) from e

    timestamp = False
    for message in fitfile.get_messages():
        try:
            timestamp = int(message.get_values()["timestamp"].timestamp())
            break
        except KeyError:
            pass
    return timestamp


def fitfile_to_df(fitfile_name):
    global config
    raw_data = []
    try:
        fitfile = fitparse.FitFile(os.path.join(config["general"]["activities_directory"],
                                                fitfile_name))
    except fitparse.utils.FitHeaderError as e:
        raise Exception("Error parsing fit file {}".format(fitfile_name)) from e

    timestamp = get_fit_timestamp(fitfile_name)
    for message in fitfile.get_messages():
        try:
            raw_data.append((timestamp,
                             sem_to_deg(message.get_values()["position_lat"]),
                             sem_to_deg(message.get_values()["position_long"])))
        except KeyError:
            pass
    return pd.DataFrame(raw_data, columns=COLUMNS)


def gpxfile_to_df(gpxfile_name):
    global config
    try:
        gpxfile = os.path.join(config["general"]["activities_directory"], gpxfile_name)
        df = Converter(input_file=gpxfile).gpx_to_dataframe()
        del df['altitude']
        df = df.rename(columns={'time': 'Timestamp', 'latitude': 'Latitude', 'longitude': 'Longitude'})
        df['Timestamp'] = int(df['Timestamp'].iloc[0].to_pydatetime().timestamp())
    except:
        raise Exception("Error parsing gpx file {}".format(gpxfile_name))
    return df


def tcxfile_to_df(tcxfile_name):
    global config
    try:
        tcxfile = os.path.join(config["general"]["activities_directory"], tcxfile_name)
        # First remove spaces
        with open(tcxfile, 'r+') as f:
            txt = f.read().lstrip(' ')
            f.seek(0)
            f.write(txt)
            f.truncate()
        tcx = tcxparser.TCXParser(tcxfile).activity
        df = pd.DataFrame(activities_to_array(tcx))
        df['Timestamp'] = int(df['Timestamp'].iloc[0].to_pydatetime().timestamp())
    except:
        raise Exception("Error parsing tsv file {}".format(tcxfile_name))
    return df


def activities_to_array(activity):
    return_array = []
    for laps in activity.Lap:
        for tracks in laps.Track:
            for trackingpoints in tracks.Trackpoint:
                dictio = {'Timestamp': dateutil.parser.parse(str(trackingpoints.Time)),
                        'Latitude': float(trackingpoints.Position.LatitudeDegrees),
                        'Longitude': float(trackingpoints.Position.LongitudeDegrees)}
                return_array.append(dictio)
            return return_array


def build_database():
    raw_data = []
    for file_name in tqdm(sorted(os.listdir(config["general"]["activities_directory"]))):
        if file_name.endswith(".fit"):
            try:
                raw_data.append(fitfile_to_df(file_name))
            except Exception as e:
                tqdm.write(str(e))
        elif file_name.endswith(".gpx"):
            try:
                raw_data.append(gpxfile_to_df(file_name))
            except Exception as e:
                tqdm.write(str(e))
        elif file_name.endswith(".tcx"):
            try:
                raw_data.append(tcxfile_to_df(file_name))
            except Exception as e:
                tqdm.write(str(e))

    data = pd.concat(raw_data,
                     ignore_index=True)
    data.to_pickle(config["general"]["database_name"])


def draw_map():
    global config
    data = pd.read_pickle(config["general"]["database_name"]).iloc[::int(config["general"]["decimate"]), :]  # Keep out_of_N row out of N
    print("Loaded data of shape {}".format(data.shape))
    the_map = folium.Map(location=[data.Latitude.median(), data.Longitude.median()],
                         zoom_start=10,
                         tiles="https://{s}.tile-cyclosm.openstreetmap.fr/cyclosm/{z}/{x}/{y}.png",
                         attr="CyclOSM")
    for activity in tqdm(data["Timestamp"].unique()):
        activity_data = data[data["Timestamp"] == activity].drop("Timestamp", axis=1)
        folium.PolyLine([tuple(r) for r in activity_data.to_numpy()],
                        color=config["style"]["color"],
                        opacity=float(config["style"]["opacity"]),
                        weight=int(config["style"]["width"])).add_to(the_map)
    the_map.save(config["general"]["map_name"])


def update_database():
    global config
    try:
        existing_data = pd.read_pickle(config["general"]["database_name"])
    except FileNotFoundError as e:
        raise Exception("Could not find the database {} to update".format(config["general"]["database_name"])) from e
    raw_new_data = []
    for file_name in tqdm(sorted(os.listdir(config["general"]["activities_directory"]))):
        if file_name.endswith(".fit"):
            try:
                if get_fit_timestamp(file_name) not in existing_data["Timestamp"].unique():
                    tqdm.write("Updating database with data from file {}".format(file_name))
                    raw_new_data.append(fitfile_to_df(file_name))
            except Exception as e:
                tqdm.write(str(e))
        elif file_name.endswith(".gpx"):
            try:
                gpxfile = os.path.join(config["general"]["activities_directory"], file_name)
                df = Converter(input_file=gpxfile).gpx_to_dataframe()
                df = df.rename(columns={'time': 'Timestamp', 'latitude': 'Latitude', 'longitude': 'Longitude'})
                timestamp = int(df['Timestamp'].iloc[0].to_pydatetime().timestamp())
                if timestamp not in existing_data["Timestamp"].unique():
                    tqdm.write("Updating database with data from file {}".format(file_name))
                    raw_new_data.append(gpxfile_to_df(file_name))
            except Exception as e:
                tqdm.write(str(e))
        elif file_name.endswith(".tcx"):
            try:
                tcxfile = os.path.join(config["general"]["activities_directory"], file_name)
                # First remove spaces
                with open(tcxfile, 'r+') as f:
                    txt = f.read().lstrip(' ')
                    f.seek(0)
                    f.write(txt)
                    f.truncate()
                tcx = tcxparser.TCXParser(tcxfile).activity
                df = pd.DataFrame(activities_to_array(tcx))
                timestamp = int(df['Timestamp'].iloc[0].to_pydatetime().timestamp())
                if timestamp not in existing_data["Timestamp"].unique():
                    tqdm.write("Updating database with data from file {}".format(file_name))
                    raw_new_data.append(tcxfile_to_df(file_name))
            except Exception as e:
                tqdm.write(str(e))

    if raw_new_data:
        new_data = pd.concat(raw_new_data,
                             ignore_index=True)
        new_data = new_data.append(existing_data,
                                   ignore_index=True)
        new_data.to_pickle(config["general"]["database_name"])
    else:
        print("No update needed")


def plot_new_roads_ratio():
    try:
        data = pd.read_pickle(config["general"]["database_name"])
    except FileNotFoundError as e:
        raise Exception("No database found, build it first".format(config["general"]["database_name"])) from e
    for timestamp in tqdm(sorted(data["Timestamp"].unique())):
        date = datetime.fromtimestamp(timestamp)
        label = "{:02d}-{:02d}-{:02d}".format(date.year, date.month, date.day)
        print(label)


cmd_to_func = {
    "build": build_database,
    "draw": draw_map,
    "update": update_database,
    "new_roads": plot_new_roads_ratio,
}

config = configparser.ConfigParser()
config.read("config.ini")

parser = ArgumentParser()
parser.add_argument("command",
                    type=str,
                    choices=[cmd for cmd in cmd_to_func.keys()])
args = parser.parse_args()

print("Command: {}".format(args.command))
cmd_to_func[args.command]()
